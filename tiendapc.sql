-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-08-2022 a las 05:50:51
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tiendapc`
--
CREATE DATABASE IF NOT EXISTS `tiendapc` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `tiendapc`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add historial', 1, 'add_historial'),
(2, 'Can change historial', 1, 'change_historial'),
(3, 'Can delete historial', 1, 'delete_historial'),
(4, 'Can view historial', 1, 'view_historial'),
(5, 'Can add rol', 2, 'add_rol'),
(6, 'Can change rol', 2, 'change_rol'),
(7, 'Can delete rol', 2, 'delete_rol'),
(8, 'Can view rol', 2, 'view_rol'),
(9, 'Can add pedidos', 3, 'add_pedidos'),
(10, 'Can change pedidos', 3, 'change_pedidos'),
(11, 'Can delete pedidos', 3, 'delete_pedidos'),
(12, 'Can view pedidos', 3, 'view_pedidos'),
(13, 'Can add factura cliente', 4, 'add_facturacliente'),
(14, 'Can change factura cliente', 4, 'change_facturacliente'),
(15, 'Can delete factura cliente', 4, 'delete_facturacliente'),
(16, 'Can view factura cliente', 4, 'view_facturacliente'),
(17, 'Can add productos', 5, 'add_productos'),
(18, 'Can change productos', 5, 'change_productos'),
(19, 'Can delete productos', 5, 'delete_productos'),
(20, 'Can view productos', 5, 'view_productos'),
(21, 'Can add tipo producto', 6, 'add_tipoproducto'),
(22, 'Can change tipo producto', 6, 'change_tipoproducto'),
(23, 'Can delete tipo producto', 6, 'delete_tipoproducto'),
(24, 'Can view tipo producto', 6, 'view_tipoproducto'),
(25, 'Can add usser', 7, 'add_usser'),
(26, 'Can change usser', 7, 'change_usser'),
(27, 'Can delete usser', 7, 'delete_usser'),
(28, 'Can view usser', 7, 'view_usser'),
(29, 'Can add log entry', 8, 'add_logentry'),
(30, 'Can change log entry', 8, 'change_logentry'),
(31, 'Can delete log entry', 8, 'delete_logentry'),
(32, 'Can view log entry', 8, 'view_logentry'),
(33, 'Can add permission', 9, 'add_permission'),
(34, 'Can change permission', 9, 'change_permission'),
(35, 'Can delete permission', 9, 'delete_permission'),
(36, 'Can view permission', 9, 'view_permission'),
(37, 'Can add group', 10, 'add_group'),
(38, 'Can change group', 10, 'change_group'),
(39, 'Can delete group', 10, 'delete_group'),
(40, 'Can view group', 10, 'view_group'),
(41, 'Can add user', 11, 'add_user'),
(42, 'Can change user', 11, 'change_user'),
(43, 'Can delete user', 11, 'delete_user'),
(44, 'Can view user', 11, 'view_user'),
(45, 'Can add content type', 12, 'add_contenttype'),
(46, 'Can change content type', 12, 'change_contenttype'),
(47, 'Can delete content type', 12, 'delete_contenttype'),
(48, 'Can view content type', 12, 'view_contenttype'),
(49, 'Can add session', 13, 'add_session'),
(50, 'Can change session', 13, 'change_session'),
(51, 'Can delete session', 13, 'delete_session'),
(52, 'Can view session', 13, 'view_session'),
(53, 'Can add categoria', 14, 'add_categoria'),
(54, 'Can change categoria', 14, 'change_categoria'),
(55, 'Can delete categoria', 14, 'delete_categoria'),
(56, 'Can view categoria', 14, 'view_categoria'),
(57, 'Can add estados', 15, 'add_estados'),
(58, 'Can change estados', 15, 'change_estados'),
(59, 'Can delete estados', 15, 'delete_estados'),
(60, 'Can view estados', 15, 'view_estados');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(3, 'pbkdf2_sha256$320000$LOij5CLpIQNRX9N0hfb33Q$PPB9CyRz9fjcFueqOAJlzVAM2p7u1O580A71vxHpZpg=', '2022-07-28 17:19:54.000000', 1, 'admin', '', '', '', 1, 1, '2022-07-28 17:19:41.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_category`, `nombre`) VALUES
(4, 'accesorios stream'),
(12, 'combos'),
(7, 'graficas'),
(8, 'mas partes'),
(5, 'pantallas'),
(3, 'perifericos'),
(6, 'sillas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(8, '2022-07-28 17:20:46.399253', '1', 'admin', 3, '', 11, 3),
(9, '2022-07-28 17:21:04.966080', '2', 'johan', 3, '', 11, 3),
(10, '2022-07-28 17:21:22.146217', '1', 'admin', 3, '', 11, 3),
(11, '2022-07-28 17:21:25.339372', '1', 'admin', 3, '', 11, 3),
(12, '2022-07-28 17:21:25.341371', '1', 'admin', 3, '', 11, 3),
(13, '2022-07-28 17:21:25.799919', '2', 'johan', 3, '', 11, 3),
(14, '2022-07-28 17:21:25.879066', '1', 'admin', 3, '', 11, 3),
(15, '2022-07-28 17:21:25.980245', '2', 'johan', 3, '', 11, 3),
(16, '2022-07-28 17:21:26.168080', '2', 'johan', 3, '', 11, 3),
(17, '2022-07-28 17:21:26.226710', '2', 'johan', 3, '', 11, 3),
(18, '2022-07-28 17:23:04.466194', '3', 'admin', 2, '[{\"changed\": {\"fields\": [\"Username\"]}}]', 11, 3),
(19, '2022-07-28 17:23:04.561140', '3', 'admin', 2, '[]', 11, 3),
(20, '2022-07-28 17:23:32.872667', '29', 'Pedidos object (29)', 2, '[{\"changed\": {\"fields\": [\"Idestado\"]}}]', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(8, 'admin', 'logentry'),
(10, 'auth', 'group'),
(9, 'auth', 'permission'),
(11, 'auth', 'user'),
(12, 'contenttypes', 'contenttype'),
(13, 'sessions', 'session'),
(14, 'tienda', 'categoria'),
(15, 'tienda', 'estados'),
(4, 'tienda', 'facturacliente'),
(1, 'tienda', 'historial'),
(3, 'tienda', 'pedidos'),
(5, 'tienda', 'productos'),
(2, 'tienda', 'rol'),
(6, 'tienda', 'tipoproducto'),
(7, 'tienda', 'usser');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-06-13 23:32:56.233746'),
(2, 'auth', '0001_initial', '2022-06-13 23:32:56.616696'),
(3, 'admin', '0001_initial', '2022-06-13 23:32:56.747977'),
(4, 'admin', '0002_logentry_remove_auto_add', '2022-06-13 23:32:56.770756'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2022-06-13 23:32:56.791290'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-06-13 23:32:56.833271'),
(7, 'auth', '0002_alter_permission_name_max_length', '2022-06-13 23:32:56.861440'),
(8, 'auth', '0003_alter_user_email_max_length', '2022-06-13 23:32:56.876263'),
(9, 'auth', '0004_alter_user_username_opts', '2022-06-13 23:32:56.884585'),
(10, 'auth', '0005_alter_user_last_login_null', '2022-06-13 23:32:56.913399'),
(11, 'auth', '0006_require_contenttypes_0002', '2022-06-13 23:32:56.916844'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2022-06-13 23:32:56.927667'),
(13, 'auth', '0008_alter_user_username_max_length', '2022-06-13 23:32:56.946190'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2022-06-13 23:32:56.961861'),
(15, 'auth', '0010_alter_group_name_max_length', '2022-06-13 23:32:56.978140'),
(16, 'auth', '0011_update_proxy_permissions', '2022-06-13 23:32:56.998316'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2022-06-13 23:32:57.013907'),
(18, 'sessions', '0001_initial', '2022-06-13 23:32:57.033996'),
(19, 'tienda', '0001_initial', '2022-06-13 23:33:08.336852'),
(20, 'tienda', '0002_alter_usser_id_rol', '2022-07-03 15:00:11.730453'),
(21, 'tienda', '0002_alter_facturacliente_idproducto', '2022-07-26 18:40:27.578439'),
(22, 'tienda', '0002_pedidos_estado', '2022-07-28 03:39:46.550951'),
(23, 'tienda', '0002_alter_pedidos_fecha_hora', '2022-07-28 17:34:46.740229');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

DROP TABLE IF EXISTS `django_session`;
CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('19ii0za7kruaot6x7r1nkaf4xskaxzb8', '.eJwVysEKwjAMANBfKTmXdbt6cngT_0A9hCW6jjUpSQuC-O_qO783YG8sLS8IhyucdUUJs5Cxh4sK6a2P42PScEIjFYQI2_8c-YWl7jwsWiBOEWYqWbI3Q1L7rdTrrkieWmYhTN07WlZPvmbTYav8hPvnCxB-LQ0:1oB3Bh:v3u7SKQAWEcNK8xuVCZQp9CqyFbnFo_AYau1eu54RcQ', '2022-07-25 23:51:37.490275'),
('1sbobxzeoiznky88vmggdid3tsmnsm7d', 'e30:1oIMH7:s8vbAFPf67s8LmPZ1QeB-mxwKrYgtFGq6bq6qRqznPk', '2022-08-15 03:39:25.216553'),
('5tc5ip3xwdoyzpyhofrgu4sfll41lfec', '.eJwVysEKwjAMANBfKTmXdbt6cngT_0A9hCW6jjUpSQuC-O_qO783YG8sLS8IhyucdUUJs5Cxh4sK6a2P42PScEIjFYQI2_8c-YWl7jwsWiBOEWYqWbI3Q1L7rdTrrkieWmYhTN07WlZPvmbTYav8hPvnCxB-LQ0:1oB41M:LIn6cKSZk_skVTfktAHgZWw--0RcP8QQh9tKuYC9oC0', '2022-07-26 00:45:00.023013'),
('5x98dufioeu8rgtp3po03aqkj9t02llm', '.eJwVysEKwjAMANBfKTmXdbt6cngT_0A9hCW6jjUpSQuC-O_qO783YG8sLS8IhyucdUUJs5Cxh4sK6a2P42PScEIjFYQI2_8c-YWl7jwsWiBOEWYqWbI3Q1L7rdTrrkieWmYhTN07WlZPvmbTYav8hPvnCxB-LQ0:1oB3L9:czdyC2ep4jX1cAbNX7Pk1fRcrCAGFJRI8ZNaalcDm1k', '2022-07-26 00:01:23.302908'),
('66q0iewjup0iwtbspzwn9cqbpmtqlif5', '.eJxVjDsOwjAQBe_iGlm24y8lPWeIdr1rHECOFCcV4u4QKQW0M_PeS4ywrXXcOi_jROIstDj9MoT84LYLukO7zTLPbV0mlHsiD9vldSZ-Xo7276BCr_s66IBeuaJxSJhzQoDgBhNRWUPxCxX7YgP56JIrSrMqDGCsM57Yknh_AORFOAA:1o0tZJ:b62k06xz5LiujWMj4sP4t7XLaw8kWEGJEv_p8vqmB24', '2022-06-27 23:34:01.061786'),
('ccaa2gcyjqh1i0cdnah7sz60enhwk01t', '.eJwNykEKAjEMBdCrDFmXKW5d6TnExafNQKRtStKAIt7deev3JcTisaSArg_6YGwF1tQ3C1CiiaI3fqPPxnvRTumS6F67DPFlqGpnyjGbonpewqMihwdM1PNk21-HHPT8_QGoEyV3:1o8u1C:DveEbuyLKTJdYqL0LM3sKvGIyrXsP48gvmac8CVi0gE', '2022-07-20 01:39:54.386850'),
('jvn1zn6vihm5ntemcp3t73z4ocr3s79x', '.eJxVjsFugzAQRH8F-YzADrEDOTXtLUq_oK3Q4rWDKXiRjaVKVf-9Rsqhvc68eZpv1kPaxj5FE3qH7MwOrPybDaA_jd8LnMDfqdLkt-CGakeqRxurV0IzPz_Yf4IR4rivG9U22AkuOo5WHbnSUjZSSCsb04EVHXTQaFTCCDuc4Ch1e1IHAKGg5bbN0uw0fnMa2PmNXWkEX1w8BhOLG3mk98S5FVS8QEDykAfTzjyZL1jW2eTjCytFyS64OO_iFgApZKpO60yAsd5cPg91igmCo1jH0QWqptXc2cfPL79NY7U:1o84Z3:ueU9NFX0Y0guJw-_htKuYsC9ZJkqSDJHPJT-1q4tK1g', '2022-07-17 18:43:25.134254'),
('o4m4634c5t4zx1ugsf0n8pini7xr3gs3', '.eJwVysEKwjAMANBfKTmXdbt6cngT_0A9hCW6jjUpSQuC-O_qO783YG8sLS8IhyucdUUJs5Cxh4sK6a2P42PScEIjFYQI2_8c-YWl7jwsWiBOEWYqWbI3Q1L7rdTrrkieWmYhTN07WlZPvmbTYav8hPvnCxB-LQ0:1oB3Ut:DWapNdamRQU4zj06rcA4vdK9K6UBbabdOksSnvalnFE', '2022-07-26 00:11:27.146485'),
('rur6re5twkttzhwcbqee27sg3obg9qr3', '.eJxVjstuwjAQRX8FeR0lthPAYUXaLW0lKhZVQdH4lTgkNvJjVfXfa1QW7XbOuXfuF-ohxbFPQfneSLRDNSr-3jiIq7J3ICewgyuFs9EbXt6V8kFD-eKkmp8e7r-CEcKY08AbzttWAdMK9AZ4vSWSiLbWEnNG2QYkEFwDUevtWtWCCtJQwViLacOIxrk0dyobjQC0-0Tn-NG9rp674-HtfXU8dZlPbgQ7OyvdOWGsidsPC5g5L15QQQt0Cgm8cdms0m12IEMVTd4LVfoloQJrFkXK6TbkBLsUSID3Jrr88fL9AyLhY0Q:1oH9y4:inLt1iEHHJmuL8rXK2ysaHm9nLOrHwo__jUFqO0YbSM', '2022-08-11 20:18:48.286332'),
('sd3loqnbtlfeba6b199a2h1hquzt1yre', '.eJwVysEKwjAMANBfKTmXdbt6cngT_0A9hCW6jjUpSQuC-O_qO783YG8sLS8IhyucdUUJs5Cxh4sK6a2P42PScEIjFYQI2_8c-YWl7jwsWiBOEWYqWbI3Q1L7rdTrrkieWmYhTN07WlZPvmbTYav8hPvnCxB-LQ0:1oB48u:qlkGg2Z2fUYXQsmxlrYV8kagxVCk_ZBY45maVKXZY1w', '2022-07-26 00:52:48.564576'),
('wqw5vrp1xtpt0yhug3zz6pq0w47joiw9', 'eyJjYXJyaXRvIjpbXX0:1oH4R1:8Xn9H0OfEFMOIH4ZD9dOQiIMvLfMg3kM3I_plU0z0iA', '2022-08-11 14:24:19.541688'),
('zkxxbvfp7urplwolmkcws8y8ulax9w3j', '.eJxVjstOwzAURH-l8jpynNTOoytg3y9AKLr2vWlcUjv1AwkQ_44jdQHbmTNH880myGmZcqQwWWQn1rDqb6bBvJPbC7yCu3huvEvBar4j_NFGfvZI68uD_SdYIC77um963Qk1N_o4amNGDdCrYztoIVscSiiom2WP3aBGNYuGxEwArVRthyR3aXGSS9YAO72yT3AHA2H18RAylLYE5oNWiPdMX083G8kBJ8zlMKuaij3jzTobUwD0ofB13lYPGOtky2eoc8wQrI_1RgH4dbuwSr79_AL4PWGF:1oGPzD:JE-NY81C9d6at8TaFO74d8Ns3iucNDurNoVePY1U-sk', '2022-08-09 19:12:55.594265');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

DROP TABLE IF EXISTS `estados`;
CREATE TABLE IF NOT EXISTS `estados` (
  `idEstado` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEstado` char(50) NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`idEstado`, `nombreEstado`) VALUES
(1, 'Creada'),
(2, 'Confirmada'),
(3, 'Enviada'),
(4, 'Cancelada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_cliente`
--

DROP TABLE IF EXISTS `factura_cliente`;
CREATE TABLE IF NOT EXISTS `factura_cliente` (
  `id_factura` int(11) NOT NULL AUTO_INCREMENT,
  `idPedido` int(11) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  `precio` int(11) UNSIGNED NOT NULL,
  `cantidad` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_factura`),
  KEY `idPedido` (`idPedido`),
  KEY `factura_cliente_IdProducto_3bc59581` (`IdProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura_cliente`
--

INSERT INTO `factura_cliente` (`id_factura`, `idPedido`, `IdProducto`, `precio`, `cantidad`) VALUES
(17, 28, 22, 89000, 1),
(18, 28, 23, 115000, 1),
(19, 28, 27, 148000, 1),
(20, 29, 22, 120000, 1),
(21, 33, 24, 129000, 1),
(22, 34, 24, 129000, 1),
(23, 35, 24, 129000, 1),
(24, 36, 23, 115000, 1),
(25, 37, 24, 129000, 1),
(26, 38, 42, 230000, 1),
(27, 39, 23, 115000, 1),
(28, 40, 23, 115000, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

DROP TABLE IF EXISTS `historial`;
CREATE TABLE IF NOT EXISTS `historial` (
  `Id_historial` int(11) NOT NULL AUTO_INCREMENT,
  `Accion` varchar(200) NOT NULL,
  `hora` datetime(6) NOT NULL,
  PRIMARY KEY (`Id_historial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE IF NOT EXISTS `pedidos` (
  `IdPedido` int(11) NOT NULL AUTO_INCREMENT,
  `Id_user` int(11) NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `fecha_hora` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `idEstado` int(11) NOT NULL,
  PRIMARY KEY (`IdPedido`),
  KEY `Id_user` (`Id_user`),
  KEY `idEstado` (`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`IdPedido`, `Id_user`, `Direccion`, `fecha_hora`, `idEstado`) VALUES
(28, 1, 'Calle 77', '2022-07-29 17:28:00', 3),
(29, 1, 'calle arriba', '2022-07-28 17:40:00', 1),
(33, 35, 'a', '2022-07-29 02:21:12', 1),
(34, 35, 'hola', '2022-07-29 03:10:33', 1),
(35, 35, 'cx', '2022-07-29 03:15:07', 1),
(36, 35, 'a', '2022-07-29 03:18:37', 1),
(37, 35, 'asd', '2022-07-29 03:19:43', 1),
(38, 35, 'asdasd', '2022-07-29 03:20:19', 1),
(39, 35, 'sadsd', '2022-07-29 03:27:01', 1),
(40, 35, 'sadsd', '2022-07-29 03:27:03', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `IdProducto` int(11) NOT NULL AUTO_INCREMENT,
  `IdTipoProducto` int(11) NOT NULL,
  `idcategory` int(11) NOT NULL,
  `NombreProducto` varchar(100) DEFAULT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `stock` int(11) UNSIGNED NOT NULL,
  `descuento` int(11) DEFAULT NULL,
  `Precio` int(11) UNSIGNED DEFAULT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`IdProducto`),
  UNIQUE KEY `NombreProducto` (`NombreProducto`),
  KEY `productos_IdTipoProducto_036b5365_fk_tipo-prod` (`IdTipoProducto`),
  KEY `idcategory` (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`IdProducto`, `IdTipoProducto`, `idcategory`, `NombreProducto`, `descripcion`, `stock`, `descuento`, `Precio`, `foto`) VALUES
(22, 49, 3, 'HYPERX PULSEFIRECORE RGB', 'El HyperX Pulsefire Core™ tiene todo lo que buscan los jugadores de verdad en un ratón para juegos RGB con cable: solidez y confort. El sensor óptico Pixart 3327 permite a los jugadores un seguimiento preciso y exacto, sin aceleración de hardware, y cuenta con ajustes nativos de ppp, de hasta 6200 p', 13, 0, 120000, 'tienda/productos/HYPERX PULSEFIRECORE RGB.png'),
(23, 27, 3, 'MOUSE MSI GM41 RGB', 'Fiabilidad de alta velocidad a 400 IPS y precisión de hasta 16000 DPI con una tasa de sondeo de 1 ms que ofrece un seguimiento extremadamente preciso. Diseño ergonómico simétrico. Mantente en control con un ajuste de forma para cualquier forma de mano con agarres laterales de goma suaves y duraderos', 10, 0, 115000, 'tienda/productos/MOUSE MSI GM41 RGB.png'),
(24, 19, 3, 'MOUSE CORSAIR M55 RGB PRO WHITE', 'M55 RGB PRO Ambidextrous\r\nóptico ultrapreciso de 12 400 ppp. con cualquier mano con el agarre más cómodo, ya sea la palma, los dedos o la punta de los dedos. para conseguir un seguimiento de gran precisión. soportar varios años de sesiones de juego intensas.', 16, 0, 129000, 'tienda/productos/MOUSE CORSAIR M55 RGB PRO WHITE.png'),
(25, 20, 3, 'MOSUE XPG PRIMER RGB', 'El XPG PRIMER tiene agarraderas laterales texturizadas con propiedades antideslizantes para una excelente tracción y un juego cómodo. Además, tiene el peso ideal (98g ± 5%) por lo que no es ni demasiado pesado ni demasiado ligero, perfectamente equilibrado para movimientos precisos.', 20, 0, 114000, 'tienda/productos/MOSUE XPG PRIMER RGB.jpg'),
(26, 24, 3, 'MOUSE RAZER DEATHADDER V2 MINI', 'Razer DeathAdder v2 Mini ratón para videojuegos: sensor óptico de 8500 K DPI – Diseño ligero de 2.19 oz – Iluminación RGB Chroma – 6 botones programables – Cinta antideslizante incluida – Negro clásico.', 20, 0, 145000, 'tienda/productos/MOUSE-RAZER-DEATHADDER-V2-MINI.jpg'),
(27, 18, 3, 'MOUSE LOGITECH G600 MMO', 'MOUSE LOGITECH G600 MMO. Todo preconfigurado para uso inmediato en juegos MMO. G600 incluye tres botones principales, 12 botones para el pulgar, un botón rueda presionable e inclinable y un botón G-Alt para doblar las opciones y dominar.', 19, 0, 148000, 'tienda/productos/LOGITECH G600 MMO.png'),
(28, 18, 3, 'LOGITECH G502 HERO KDA', 'MOUSE GAMER LOGITECH G502 HERO KDA BLANCO RGB LIGHT. G502 HERO dispone de un sensor óptico avanzado para máxima precisión de seguimiento, iluminación RGB personalizada, perfiles de juego personalizados, de 200 a 25.600 dpi y pesas reposicionables.', 20, 0, 189000, 'tienda/productos/LOGITECH G502 HERO KDA.png'),
(29, 74, 3, 'MOUSE STEELSERIES SENSEI TEN', 'Ratón Gamer con cable - ambidiestro - sensor óptico de 18000 dpi - 8 botones programables - retroiluminación RGB. Con su icónica forma de ratón Sensei y su increíble rendimiento, el SteelSeries Sensei Ten se convertirá rápidamente en una necesidad para tus sesiones de gaming.', 20, 0, 199000, 'tienda/productos/MOUSE STEELSERIES SENSEI TEN.jpg'),
(30, 27, 3, 'TECLADO MSI GAMING VIGOR GK20 US', 'El diseño hidrófugo hace que el teclado GK20 sea más durable y fácil de limpiar. No sumergir el teclado en agua ni en ningún líquido. Repelente al agua: Pruebas bajo condiciones limitadas(máximo 60 ml de salpicaduras de líquido), excluyendo las teclas numéricas y el indicador en la esquina superior ', 20, 0, 118000, 'tienda/productos/TECLADO MSI GAMING VIGOR GK20 US_w1hxPZm.jpg'),
(31, 20, 3, 'TECLADO XPG INFAREX K10', 'Color: Negro\r\nEfectos luminosos: 9\r\nNúmero de teclas: 104\r\nVida útil en pulsaciones: >10 millones\r\nNúmero pulsaciones simultáneas de teclas reconocidas: 26\r\nPuerto USB: USB Tipo-A\r\nVoltaje de trabajo: 5 VCC\r\nCorriente nominal: < 500mA', 20, 0, 109000, 'tienda/productos/TECLADO XPG INFAREX K10.png'),
(32, 42, 3, 'COOLER MASTER DEVASTATOR 3 PLUS', 'Un combo de juegos con color\r\nEl mouse ergonómico ultraplano cuenta con un DPI ajustable de hasta 2400 y una rueda de mouse de alta precisión, mientras que el teclado de bajo perfil viene con teclas con recubrimiento de agarre grabadas con láser e interruptores Mem-chanical para una mejor respuesta ', 20, 0, 175000, 'tienda/productos/COOLER MASTER.png'),
(33, 74, 3, 'STEELSERIES APEX 3 TKL US', 'SteelSeries Apex 3 TKL RGB - Teclado para juegos – Factor de forma compacto sin llave – Iluminación RGB de 8 zonas – IP32 resistente al agua y al polvo – Interruptor silencioso para juegos – Grado de juego Anti-ghosting.', 20, 0, 199000, 'tienda/productos/STEELSERIES APEX 3 TKL US.jpg'),
(34, 74, 3, 'STEELSERIES APEX 3 SP', 'Interruptores silenciosos para juegos: uso casi silencioso para 20 millones de pulsaciones de teclas de baja fricción. Reposamuñecas magnético de alta calidad: proporciona apoyo completo y comodidad en la palma. Controles multimedia específicos: ajusta el volumen y los ajustes sobre la marcha.', 20, 0, 210000, 'tienda/productos/STEELSERIES-APEX-3-SP.png'),
(35, 18, 3, 'LOGITECH K835 TKL BLACK', 'La sensación de escritura mecánica que tanto te gusta. Duradero, moderno y compacto, con un diseño sin sección numérica, listo para combinar con el resto de tu equipo. Los interruptores mecánicos te ofrecen precisión con la respuesta deseada.', 20, 0, 225000, 'tienda/productos/LOGITECH-K835-TKL-BLACK.png'),
(36, 30, 3, 'TRUST GXT 865 MECANICO RGB', 'Descripción. El largo camino a la victoria es más fácil con los interruptores GXT-RED mecánicos Linear de respuesta rápida, que tienen una duración de hasta 50 millones de pulsaciones. La tasa de sondeo de hasta 1000 Hz y un tiempo de respuesta de hasta 1 ms te darán la velocidad de un profesional.', 20, 0, 250000, 'tienda/productos/TRUST-GXT-865-MECANICO-RGB.png'),
(37, 18, 3, 'TECLADO LOGITECH G413 MECANICO', 'La compacta carcasa es de aleación de aluminio 5052 ligera y altamente resistente. En la base hay un sistema de gestión de cables de mouse y audífonos, y patas ajustables con estabilizadores de goma para la colocación en el ángulo de uso más adecuado.', 20, 0, 309000, 'tienda/productos/TECLADO-LOGITECH-G413-MECANICO.png'),
(41, 75, 4, 'Diadema Gamer Usb Gamer Tech Kx15', 'Tus juegos, tus sonidos, tu música, tu vida! Los auriculares perfectos para disfrutar el sonido, con almohadillas de excelente calidad que te permitirán usarlos durante horas y micrófono para comunicarte en las sesiones de juego en línea.', 20, 0, 120000, 'tienda/productos/Diadema Gamer Usb Gamer Tech Kx15.png'),
(42, 76, 4, 'G332 DIADEMA GAMER MULTIPLAY - LOGI', 'Los grandes transductores de audio de 50 mm producen un sonido completamente expansivo para una experiencia de juego más inmersiva. Por fin tus juegos favoritos sonarán como se concibieron: asombrosamente. El micrófono de 6 mm garantiza que tus compañeros de escuadrón puedan oírte.', 18, 0, 230000, 'tienda/productos/G332-DIADEMA-GAMER-MULTIPLAY-LOGI.jpg'),
(43, 76, 4, 'G335 MULTIPLATAFORMA - LOGI', 'Los G335 – Logitech – Negro son unos auriculares ligeros, finos y cómodos, con una suave diadema elástica que se adapta perfectamente a tu cabeza y que se puede ajustar para conseguir el mejor ajuste. También puedes deslizar los cascos hacia arriba o hacia abajo para ajustar el auricular.', 20, 0, 330000, 'tienda/productos/G332-DIADEMA-GAMER-MULTIPLAY-LOGI_iHcjhkw.jpg'),
(44, 76, 4, 'LOL PRO X EDICION ESPECIAL - LOGI', 'Presentamos una edición exclusiva de nuestros audífonos con micrófono PRO X para juegos. Los revestimos con los emblemáticos colores de League of Legends. Así podrás sumergirte completamente en tu juego favorito.', 19, 0, 700000, 'tienda/productos/LOL PRO X EDICION ESPECIAL.png'),
(45, 77, 4, 'MICROFONO STREAMER - THRONMAX', 'Patrón dual, cardioid y cancelación de ruido, soporte de 360 ​​grados\r\nCancelación activa de ruido que reduce el ruido de fondo y proporciona un sonido nítido y claro\r\nGrabación de grado de estudio de alta definición 96 kHz 24 bits, botón de silencio, perilla de control de volumen\r\nMonitorización de', 20, 0, 230000, 'tienda/productos/MICROFONO STREAMER - THRONMAX.png'),
(46, 24, 4, 'MICROFONO SEIREN MINI USB - RAZER', 'Calidad de grabación profesional: con su cápsula de condensador de 0.551 in y respuesta de frecuencia plana, el micrófono transmite tu voz con claridad estelar que está llena de agudos nítidos y bajos profundos.', 20, 0, 270000, 'tienda/productos/MICROFONO SEIREN MINI USB - RAZER.png'),
(47, 30, 4, 'MICROFONO PRO FYRU LED - GXT', 'Calidad de audio excelenteDe todos los elementos que utiliza en sus grabaciones, el micrófono es el que tiene mayor efecto. Tiene que capturar voces e instrumentos con la calidad más alta posible. Por eso, el micrófono para streaming 4 en 1 GXT 258 Fyru de Trust se ha concebido para ofrecer grabacio', 20, 0, 460000, 'tienda/productos/MICROFONO PRO FYRU LED - GXT.png'),
(48, 18, 4, 'MICROFONO SNOWBALL BLACK - BLUE', 'Snowball iCE es la forma más rápida y sencilla de conseguir un sonido de alta calidad para grabaciones y streaming. Alimentado por una cápsula de condensador cardioide personalizada, Snowball iCE proporciona una calidad de audio transparente como el cristal que está a años luz del micrófono incorpor', 20, 0, 295000, 'tienda/productos/MICROFONO SNOWBALL BLACK - BLUE.png'),
(50, 52, 12, 'combo gamer core i3 10100 video rx550 8ram ssd 240 fuente real 500 monitor 22 full hd teclado y mous', 'combo gamer core i3 10100 video rx550 8ram ssd 240 fuente real 500 monitor 22 full hd teclado y mouse gamer', 20, 0, 3300000, 'tienda/productos/combo intel3_pvuEmSx.jpg'),
(51, 48, 12, 'Combo Gamer Ryzen 5 3500, 1650 4gb, Ram 8, SSD 240, Fuente Real, Monitor 22', 'Combo Gamer Ryzen 5 3500, 1650 4gb, Ram 8, SSD 240, Fuente Real, Monitor 22', 20, 0, 3600000, 'tienda/productos/combo3 53500_f9Sz3bH.png'),
(52, 52, 12, 'Combo i5 grafica gt 710', 'intel 5\r\ngt 710\r\n1tb\r\n254gb solido\r\n8 ram', 20, 0, 3000000, 'tienda/productos/combgt710o.png'),
(53, 48, 12, 'Combo athlon', 'ryzen athlon\r\n8 ram\r\ngrafico integrados\r\npantalla 20\r\nmouse,mouse pad y teclado gamer\r\n', 20, 0, 2000000, 'tienda/productos/combo-athlon.png'),
(54, 48, 12, 'Combo Gamer ryzen 3 3100, RX 550', 'Combo Gamer ryzen 3 3100, RX 550, 8 Ram, SSD 240, Monitor 22 Full HD, Teclado y Mouse Gamer', 20, 0, 2800000, 'tienda/productos/comboryzen_3_3100.png'),
(55, 78, 5, 'Monitor Gamer Curvo 165HZ 1ms | AOC CQ32G2S QHD 2K', 'Sin marco AOC CQ32G2S QHD 2K de 32 «2K QHD, VA curvo 1500R, 1 ms, 165 Hz, FreeSync. Monitor Gamer Curvo 165hz Gaming', 20, 0, 2400000, 'tienda/productos/Monitor-Gamer-Curvo-165HZ-1ms.png'),
(56, 27, 5, 'MONITOR GAMER MSI MAG270VC2 FHD 1MS 27 CURVO 165HZ', 'MODELO: Optix MAG270VC2\r\n\r\nTamaño (pulg,) : 27″\r\nResolución Máxima : 1920×1080\r\nRatio de Aspecto : 16:09\r\nVelocidad de Respuesta : 1ms\r\nFrecuencia Actualizacion : 165Hz\r\nÁngulo de Visión : 178º(H), 178º(V)\r\n', 20, 0, 1800000, 'tienda/productos/MONITOR-GAMER-MSI.png'),
(57, 14, 5, 'MONITOR SAMSUNG FHD 1920-1080 24F354 24″', 'MODELO: LS24F354FHLXPE\r\n\r\nPantalla Tamaño : 24 Pulg\r\nTipo : Led\r\nProporción : Wide\r\nResolución: Max 1920 X 1080\r\nContraste : 1 000:1\r\nContraste Dinámico : Mega\r\nBrillo : 250 Cd/M2\r\nAngulo De Visión: 178º / 178º\r\nTiempo De Respuesta: 4 Ms\r\n', 20, 0, 1200000, 'tienda/productos/MONITOR-SAMSUNG-FHD-1920-1080-24F354-24-1-600x550.png'),
(58, 31, 5, 'MONITOR LED 23.8» IPS MK600H', 'MODELO: 24MK600M-B\r\n\r\nDiseño prácticamente sin bordes en los 3 lados\r\nRadeon FreeSync™\r\nPanel IPS\r\nResolución 1920×1080 Full HD\r\nFrecuencia 75Hz\r\nTiempo de respuesta 5ms\r\nHDMI / VGA / Audio.\r\n', 20, 0, 900000, 'tienda/productos/MONITOR-LED-23.8_-IPS-MK600H-1-600x550.png'),
(59, 35, 5, 'MONITOR 27″ GIGABYTE M27F FHD 144HZ 1MS', '\r\n\r\n\r\n\r\nMONITOR 27″ GIGABYTE M27F FHD 144HZ 1MS\r\nEn Stock\r\nMODELO: M27F\r\n\r\nTamaño del panel (diagonal): 27 «IPS\r\nÁrea de visualización de pantalla (HxV): 597,6 x 336,15 (milímetro)\r\nSaturación de color: 95% DCI-P3 / 130% sRGB\r\nTamaño de píxel: 0,31125 (H) x 0,31125 (V) (mm)\r\nBrillo: 300 cd / m2 (TIP', 20, 0, 3100000, 'tienda/productos/MONITOR-27-GIGABYTE-M27F-FHD-144HZ-1MS-1-1-600x550.png'),
(60, 79, 6, 'Silla Gamer Iceberg GC80 / Material Premium / Reposabrazos Acolchado / MORADA', 'Diseño ergonómico con soporte lumbar\r\nMaterial premium de cuero PU\r\nReposa brazos almohadilla\r\nPistón de gas clase 4, soporta hasta 150 Kg\r\nCompletamente ajustable, reclina de 90 a 180 grados\r\nIncluye cojines para soporte lumbar y cabeza\r\nEstructura en acero', 20, 0, 1200000, 'tienda/productos/sillamorada.png'),
(61, 79, 6, 'Silla Gamer Iceberg GC80 / Material Premium / Reposabrazos Acolchado / blanca', 'Diseño ergonómico con soporte lumbar\r\nMaterial premium de cuero PU\r\nReposa brazos almohadilla\r\nPistón de gas clase 4, soporta hasta 150 Kg\r\nCompletamente ajustable, reclina de 90 a 180 grados\r\nIncluye cojines para soporte lumbar y cabeza\r\nEstructura en acero', 20, 0, 1200000, 'tienda/productos/sillablanca.png'),
(62, 79, 6, 'Silla Gamer Iceberg GC80 / Material Premium / Reposabrazos Acolchado / Azul', 'Diseño ergonómico con soporte lumbar\r\nMaterial premium de cuero PU\r\nReposa brazos almohadilla\r\nPistón de gas clase 4, soporta hasta 150 Kg\r\nCompletamente ajustable, reclina de 90 a 180 grados\r\nIncluye cojines para soporte lumbar y cabeza\r\nEstructura en acero', 20, 0, 1200000, 'tienda/productos/sillaazul.png'),
(63, 79, 6, 'Silla Gamer Iceberg GC80 / Material Premium / Reposabrazos Acolchado / Roja', 'Diseño ergonómico con soporte lumbar\r\nMaterial premium de cuero PU\r\nReposa brazos almohadilla\r\nPistón de gas clase 4, soporta hasta 150 Kg\r\nCompletamente ajustable, reclina de 90 a 180 grados\r\nIncluye cojines para soporte lumbar y cabeza\r\nEstructura en acero', 20, 0, 1200000, 'tienda/productos/sillaroja.png'),
(64, 79, 6, 'Silla Gamer Iceberg GC80 / Material Premium / Reposabrazos Acolchado / rosada', 'Diseño ergonómico con soporte lumbar\r\nMaterial premium de cuero PU\r\nReposa brazos almohadilla\r\nPistón de gas clase 4, soporta hasta 150 Kg\r\nCompletamente ajustable, reclina de 90 a 180 grados\r\nIncluye cojines para soporte lumbar y cabeza\r\nEstructura en acero', 20, 0, 1200000, 'tienda/productos/sillarosada.png'),
(65, 47, 7, 'ZOTAC GAMING GeForce GTX 1650 AMP! Twin Fan 4GB GDDR6', 'ZOTAC GAMING GeForce GTX 1650 AMP! Twin Fan 4GB GDDR6', 20, 0, 1199000, 'tienda/productos/gtx 1650 zotac.png'),
(66, 47, 7, 'ZOTAC GAMING GeForce RTX 3050 Twin Edge 8GB GDDR6', 'ZOTAC GAMING GeForce RTX 3050 Twin Edge 8GB GDDR6', 20, 0, 1899000, 'tienda/productos/rx3050.png'),
(67, 47, 7, 'ZOTAC GAMING GeForce RTX 3060 Ti Twin Edge 8GB GDDR6', 'ZOTAC GAMING GeForce RTX 3060 Ti Twin Edge 8GB GDDR6', 20, 0, 2910000, 'tienda/productos/rtx-3060-ti-twin-edge-01.png'),
(68, 47, 7, 'ZOTAC GAMING GeForce RTX 3070 Twin Edge 8GB GDDR6', 'ZOTAC GAMING GeForce RTX 3070 Twin Edge 8GB GDDR6', 20, 0, 3500000, 'tienda/productos/zotac-rtx-3070-twin-edge-2.png'),
(69, 47, 7, 'ZOTAC GAMING GeForce RTX 3080 Ti Trinity 12GB GDDR6X', 'ZOTAC GAMING GeForce RTX 3080 Ti Trinity 12GB GDDR6X', 20, 0, 6850000, 'tienda/productos/3080Ti-1-GTECH-700x700.png'),
(70, 79, 8, 'Chasis Caja Iceberg Glacius Vidrio Templado + Ventilador Led 120mm', '► Panel Lateral en Vidrio Templado\r\n► Incluye 1 ventilador 120mm led Rojo\r\n► Estructura (soporte Boards): ATX, m-ATX / ITX\r\n► Soporte Radiador 120mm atrás\r\n► Tarjetas Gráficas hasta 280mm de largo\r\n► Soporte para manejo de cables\r\n► Soporta Cooler para CPU de hasta 160mm\r\n► 1 x USB 3.0, 2 x USB 2.0,', 20, 0, 220000, 'tienda/productos/Glacius-Black_sm.png'),
(71, 79, 8, 'Caja Chasis Aerocool Bolt RGB Ventana Acrílico', 'Factor de Forma: Mid Tower.\r\nAcrílico Completo.\r\nSoporte Board ATX, Micro ATC, Mini ITX.\r\nCantidad de puertos USB2.0 (2).\r\nBahías 3,5” (2).\r\nBahías 2,5” (2).\r\nRanuras de expansión (7).\r\nEspacio GPU hasta 355mm.\r\nEspacio cooler de CPU hasta 155 mm de altura.\r\nDimensiones caja (general) 194 x 444 x 41', 20, 0, 230000, 'tienda/productos/Bolt-Infographic700x700-02.png'),
(72, 79, 8, 'Caja Chasis Aerocool Rift Tira RGB + Ventilador 120mm Ventana Vidrio', 'Factor de Forma: Mid Tower.\r\nAcrílico Completo.\r\nSoporte Board ATX, Micro ATC, Mini ITX.\r\nCantidad de puertos USB2.0 (2).\r\nBahías 3,5” (2).\r\nBahías 2,5” (2).\r\nRanuras de expansión (7).\r\nEspacio GPU hasta 355mm.\r\nEspacio cooler de CPU hasta 155 mm de altura.\r\nDimensiones caja (general) 194 x 444 x 41', 20, 0, 230000, 'tienda/productos/Rift-Product-Photo-Gallery4-1.png'),
(73, 79, 8, 'Chasis Caja Iceberg Mech One – 5 Ventiladores + Doble Tira LED M-RGB + 2 Vidrios con Protector Metál', '► Diseño exclusivo y vanguardista\r\n► Espacio amplio para montaje y flujo de aire\r\n► Acero SPCC de 1.0mm con blindaje EMI reforzado\r\n► Soporte de tarjetas de video de hasta 330mm\r\n► Manejo de cableado apropiado para flujo de aire\r\n► 2 x USB 3.0 + HD Audio, puertos ubicados en el panel superior\r\n► 4 V', 20, 0, 1200000, 'tienda/productos/main1.png'),
(74, 40, 8, 'Refrigeración Líquida Aerocool Mirage L360 aRGB Infinito Radiador Triple de 360mm', 'Refrigeración líquida con radiador de 360mm\r\nTriple ventilador 120mm ARGB\r\nBloque ARGB con efecto infinito de iluminación\r\nTDP de hasta 550W de resistencia térmica\r\nRGB (ASUS Aura Sync, MSI Mystic Light Sync, Gigabyte RGB Fusion, y ASRock RGB Sync) 5V 3Pin direccionable', 20, 0, 480000, 'tienda/productos/Mirage-L360-Infographic700x700px-02.png'),
(75, 40, 8, 'Disipador de Calor PCCOOLER GI-D56A Ventilador ARGB de 120mm', 'Disipador de calor con 5 heat pipes\r\nVentilador 120mm Iluminación ARGB Direccionable\r\nCubierta con iluminación RGB\r\nTDP de hasta 160W de resistencia térmica', 15, 0, 260000, 'tienda/productos/gi-d56A-1-Case-Conflict.png'),
(76, 40, 8, 'Disipador de Calor PCCOOLER GI-D66A Doble Ventilador ARGB de 120mm', 'Disipador de calor con 6 heat pipes\r\nDoble ventilador 120mm Iluminación ARGB Direccionable\r\nCubierta con iluminación RGB\r\nTDP de hasta 230W de resistencia térmica', 20, 0, 350000, 'tienda/productos/gi-d66A-1-Case-Conflict.png'),
(77, 40, 8, 'Refrigeración Líquida Aerocool Mirage L120 aRGB Infinito Radiador 120mm', 'Refrigeración líquida con radiador de 120mm\r\nVentilador de 120mm ARGB\r\nBloque ARGB con efecto infinito de iluminación\r\nTDP de hasta 200W de resistencia térmica\r\nRGB (ASUS Aura Sync, MSI Mystic Light Sync, Gigabyte RGB Fusion, y ASRock RGB Sync) 5V 3Pin direccionable', 20, 0, 320000, 'tienda/productos/Mirage-L120-Infographic700x700px-01.png'),
(78, 40, 8, 'Fuente de Poder Real Aerocool Cylon 700 RGB 700W Certificada 80 Plus Bronze', 'Fuente de poder real serie Aerocool Cylon RGB\r\nPotencia de 700W reales\r\nCertificación 80+ Bronze\r\n85% de eficiencia\r\nIluminación aRGB\r\nVentilador de 120mm\r\nManejo de cables optimizado con cables negro y planos', 20, 0, 320000, 'tienda/productos/AeroCool-Cylon-psu2.png'),
(79, 79, 8, 'Fuente de Poder Real Iceberg 600-LX 600W OEM', 'Fuente de Poder Real Iceberg 600-LX 600W OEM', 20, 0, 200000, 'tienda/productos/3-1.png'),
(80, 79, 8, 'Fuente de Poder Real Iceberg 600-BX 600W Certificada 80 Plus Bronze', 'Fuente de Poder Real Iceberg 600-BX 600W Certificada 80 Plus Bronze', 20, 0, 250000, 'tienda/productos/1-1.png'),
(81, 79, 8, 'Fuente de Poder Real Iceberg 450-LS', 'Fuente de Poder Real Iceberg 450-LS', 20, 0, 290000, 'tienda/productos/2.png'),
(82, 20, 8, 'Memoria Ram Ddr4 8gb Adata Xpg Rgb Spectrix D41 3200mhz', 'Memoria Ram Ddr4 8gb Adata Xpg Rgb Spectrix D41 3200mhz', 20, 0, 190000, 'tienda/productos/12.png'),
(83, 20, 8, 'Memoria Ram Ddr4 16gb Adata Xpg Rgb Spectrix D50 3200mh', 'Memoria Ram Ddr4 16gb Adata Xpg Rgb Spectrix D50 3200mh', 20, 0, 360000, 'tienda/productos/D50.png'),
(84, 19, 8, 'Memoria Ram PC DDR4 8Gb Corsair Vengeance RGB RS 3200', 'Memoria Ram PC DDR4 8Gb Corsair Vengeance RGB RS 3200', 20, 0, 220000, 'tienda/productos/CORSAIR-RS.png'),
(85, 81, 8, ' Memoria Ram PC DDR4 16Gb Kingston Fury Beast RGB 3200', '\r\nMemoria Ram PC DDR4 16Gb Kingston Fury Beast RGB 3200', 20, 0, 380000, 'tienda/productos/BEAST-RGB.PNG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `Id_Rol` int(11) NOT NULL AUTO_INCREMENT,
  `NameRol` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Rol`),
  UNIQUE KEY `NameRol` (`NameRol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`Id_Rol`, `NameRol`) VALUES
(1, 'Administrador'),
(2, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo-producto`
--

DROP TABLE IF EXISTS `tipo-producto`;
CREATE TABLE IF NOT EXISTS `tipo-producto` (
  `IdTipoProducto` int(11) NOT NULL AUTO_INCREMENT,
  `Marca` varchar(100) NOT NULL,
  PRIMARY KEY (`IdTipoProducto`),
  UNIQUE KEY `Marca` (`Marca`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo-producto`
--

INSERT INTO `tipo-producto` (`IdTipoProducto`, `Marca`) VALUES
(79, '	ICEBERG'),
(40, 'AEROCOOL'),
(78, 'AOC'),
(36, 'AORUS'),
(29, 'ASTRO'),
(5, 'ASUS'),
(37, 'BENQ'),
(34, 'CMAX'),
(43, 'COOLER MASTER'),
(19, 'CORSAIR'),
(26, 'COUGAR'),
(41, 'DXT'),
(28, 'FANTECH'),
(35, 'GIGABYTE'),
(30, 'GXT'),
(49, 'HYPERX'),
(52, 'INTEL'),
(45, 'INTIMATE WM HEART'),
(81, 'KINGSTON'),
(31, 'LG'),
(76, 'LOGI'),
(18, 'LOGITECH'),
(42, 'MASTER'),
(27, 'MSI'),
(46, 'PALIT'),
(39, 'POWER'),
(24, 'RAZER'),
(21, 'REDRAGON'),
(48, 'RYZEN'),
(14, 'SAMSUNG'),
(51, 'SEAGATE'),
(44, 'SONGMICS'),
(38, 'SONY'),
(74, 'STEELSERIES'),
(23, 'T DAGGER'),
(75, 'TECH'),
(22, 'THERMALTAKE'),
(77, 'THRON'),
(50, 'TOSHIBA'),
(25, 'VSG'),
(20, 'XPG'),
(47, 'ZOTAC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usser`
--

DROP TABLE IF EXISTS `usser`;
CREATE TABLE IF NOT EXISTS `usser` (
  `Id_user` int(11) NOT NULL AUTO_INCREMENT,
  `NumeroDocumento` int(10) UNSIGNED DEFAULT NULL,
  `userName` varchar(200) DEFAULT NULL,
  `UserEmail` varchar(100) DEFAULT NULL,
  `Celular` int(10) UNSIGNED DEFAULT NULL,
  `Pass_word` varchar(10) DEFAULT NULL,
  `foto` varchar(100) NOT NULL,
  `id_rol` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_user`),
  UNIQUE KEY `NumeroDocumento` (`NumeroDocumento`),
  UNIQUE KEY `UserEmail` (`UserEmail`),
  UNIQUE KEY `Celular` (`Celular`),
  UNIQUE KEY `NumeroDocumento_2` (`NumeroDocumento`),
  UNIQUE KEY `userName` (`userName`),
  KEY `usser_id_rol_d901f70b_fk_rol_Id_Rol` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usser`
--

INSERT INTO `usser` (`Id_user`, `NumeroDocumento`, `userName`, `UserEmail`, `Celular`, `Pass_word`, `foto`, `id_rol`) VALUES
(1, 1001789546, 'Johan andres londoño cardona', 'johalondoo1@gmail.com', 3014649274, 'tono1234', 'tienda/usuarios/shiro.jpeg', 1),
(28, 123456789, '	YAN CARLOS RUA', 'johanlondoño@gmail.com', 3126225577, 'johan123', 'tienda/usuarios/anime1.jpg', 2),
(35, 1001753951, 'tonower', 'yan@gmail.com', 12345678, '12345678', 'tienda/usuarios/per.jfif', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `factura_cliente`
--
ALTER TABLE `factura_cliente`
  ADD CONSTRAINT `factura_cliente_IdProducto_3bc59581_fk_productos_IdProducto` FOREIGN KEY (`IdProducto`) REFERENCES `productos` (`IdProducto`),
  ADD CONSTRAINT `factura_cliente_ibfk_1` FOREIGN KEY (`idPedido`) REFERENCES `pedidos` (`IdPedido`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_Id_user_dd48ca26_fk_usser_Id_user` FOREIGN KEY (`Id_user`) REFERENCES `usser` (`Id_user`),
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`idEstado`) REFERENCES `estados` (`idEstado`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_IdTipoProducto_036b5365_fk_tipo-prod` FOREIGN KEY (`IdTipoProducto`) REFERENCES `tipo-producto` (`IdTipoProducto`),
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`idcategory`) REFERENCES `categoria` (`id_category`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usser`
--
ALTER TABLE `usser`
  ADD CONSTRAINT `usser_id_rol_d901f70b_fk_rol_Id_Rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`Id_Rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
